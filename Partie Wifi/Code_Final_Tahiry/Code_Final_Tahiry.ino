#include <LTask.h>
#include <LWiFi.h>
#include <LWiFiClient.h>

 

void setup() {
  LTask.begin();
  LWiFi.begin();
  //Initialize serial and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ;// Wait for serial port to connect. Needed for native USB port only
  }
}

void loop() {
  // Scan for existing networks:
  Serial.println("Scanning available networks...");
  listNetworks();
  delay(10000);
}

void listNetworks() {
  
  // Scan for nearby networks:
  Serial.println("** Scan Networks **");
  int numSsid = LWiFi.scanNetworks();
  if (numSsid == -1) {
    Serial.println("Couldn't get a wifi connection");
    while (true);
  }

  // Print the list of networks seen:
  Serial.print("Number of available networks: ");
  Serial.println(numSsid);
  
  // Print the network number and name for each network found:
  for (int thisNet = 0; thisNet < numSsid; thisNet++) {
    Serial.print(thisNet); // Numéro du réseau
    Serial.print(") "); 
    Serial.print(LWiFi.SSID(thisNet)); //Nom du réseau
    Serial.print("\tSignal: ");
    Serial.print(LWiFi.RSSI(thisNet)); //Signal du réseau
    Serial.print(" dBm");
    printEncryptionType(thisNet);

    if (LWiFi.SSID(thisNet) == "RT-WIFI-Guest") {
      #define WIFI_AP "RT-WIFI-Guest"
      #define WIFI_PASSWORD "wifirtguest"
      #define WIFI_AUTH LWIFI_WPA
      LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD));
      //#define WIFI_AP "Fabuenoo"
      //#define WIFI_PASSWORD "dadadada"
      //#define WIFI_AUTH LWIFI_WPA
      //WPA2PSK
      //comment : il reconnait le cryptage wpa2psk comme un réseau ouvert
    }

  // Adresse MAC du routeur attaché:
    byte bssid[6];
    LWiFi.BSSID(bssid);    
    Serial.print("\tAdresse MAC : ");
    Serial.print(bssid[5],HEX);
    Serial.print(":");
    Serial.print(bssid[4],HEX);
    Serial.print(":");
    Serial.print(bssid[3],HEX);
    Serial.print(":");
    Serial.print(bssid[2],HEX);
    Serial.print(":");
    Serial.print(bssid[1],HEX);
    Serial.print(":");
    Serial.println(bssid[0],HEX);
  }
}

void printEncryptionType(int thisType) {
  // read the encryption type and print out the name:
  switch (thisType) {
    case LWIFI_WEP:
      Serial.println("\tEncryption: WEP");
      break;
    case LWIFI_WPA:
      Serial.println("\tEncryption: WPA");
      break;
    case LWIFI_OPEN:
      Serial.println("\tEncryption: Open");
      break;
    default :
      Serial.println("\tEncryption: Other");
      break;
  }
}
