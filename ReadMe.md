## Projet m4207

This project is an IoT project that allow you to find where is a key.
It use LinkitOne, to wich a battery, a battery meter, a GPS and a SIM card are connected.
When a key is lost, a text message is send to a specified mobile number (specified in SMS_Code.ino).

Indeed when that the student borrows the key, he will owe the empreunter for a number of times defines, at the end of this time if the student does not still have to return the key, 
the system will send its GPS position via SMS to the secretarial department where the student got back the key.
The key will send its latitude and its longitude what will allow to localize it.

For the management of the battery we realized a rather simple system with leds 2 one blue and the other red. 
When the battery is upper to 20 % the led blue will flash and when the battery will be low and
when it will be necessary reloaded it, the red led will begin flashing.

