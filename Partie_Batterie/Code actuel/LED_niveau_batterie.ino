#include <LBattery.h>
#define LED 2 //connect LED to digital pin2
#define LED2 3

char buff[256];

void setup() {                
  // initialize the digital pin2 as an output.
  pinMode(LED, OUTPUT); 
  pinMode(LED2, OUTPUT);    
}
 
void loop() {
  sprintf(buff,"battery level = %d", LBattery.level() );//We post the percentage of the battery via the serial port
  Serial.println(buff);
  sprintf(buff,"is charging = %d",LBattery.isCharging() );
  Serial.println(buff);
  
  if ( LBattery.level() > 20){// When the battery level is HIGH first led flash
  digitalWrite(LED, HIGH);// set the LED on
  delay(1000);
  
  digitalWrite(LED, LOW);// set the LED off
  delay(100);
  
  }
  else if ( LBattery.level()< 20) {// When the battery level is LOW second led flash
     digitalWrite(LED2, HIGH);// set the LED on
  delay(100);
  
  digitalWrite(LED2, LOW);// set the LED off
  delay(50);
    
  }
  delay(1000);
}

